//
//  DallasBuyer.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 04/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import UIKit

/**
 Interface represents member of club
 */
protocol DallasBuyer {
  var name: String { get }
  var avatarPathURL: String { get }
  var source: String { get }
}
