//
//  DailyMotionService.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 01/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import RxSwift
import Moya

/**
Interface for calling DailyMotion API
*/
protocol DailyMotionServiceType {
  func get() -> Observable<DailyMotion>
}

/**
Serves api provider to calling GitHub.
Use DailyMotionServiceType to get specific endpoints
*/
class DailyMotionService {
  
  private(set)var api: DailyMotionApiProvider
  
  init(api: DailyMotionApiProvider = DailyMotionApiProvider()) {
    self.api = api
  }
}

extension DailyMotionService: DailyMotionServiceType {
  func get() -> Observable<DailyMotion> {
    let path = DailyMotionApi.getUsers
    
    return api
      .rxRequest(path)
      .map { (response) -> DailyMotion in
        guard let data = DailyMotion.decode(from: response.data) else {
          SLog.e("Decode failure")
          fatalError()
        }
        return data
    }
    .asObservable()
  }
}
