//
//  GitHubService.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 01/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import Moya
import RxSwift

/**
 Interface for calling GitHub API
 */
protocol GitHubServiceType {
  func get() -> Observable<GitHubUsers>
}

/**
 Serves api provider to calling GitHub.
 Use GitHubServiceType to get specific endpoints
 */
class GitHubService {
  
  private(set)var api: GitHubApiProvider
  
  init(api: GitHubApiProvider = GitHubApiProvider()) {
    self.api = api
  }
}

extension GitHubService: GitHubServiceType {
  func get() -> Observable<GitHubUsers> {
    let path = GitHubApi.getUser
    
    return api
      .rxRequest(path)
      .map { (response) -> GitHubUsers in
        guard let data = GitHubUsers.decode(from: response.data) else {
          SLog.e("Decode failure")
          fatalError()
        }
        return data
    }
    .asObservable()
  }
}
