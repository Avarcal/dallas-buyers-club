//
//  DailyMotionApi.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 30/09/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import Moya

/**
 Provider to requesting Daily Motion Api
 */
typealias DailyMotionApiProvider = MoyaProvider<DailyMotionApi>

/**
 This target provide all cases to uses DailyMotion Api
 */
enum DailyMotionApi {
  case getUsers
}

extension DailyMotionApi: TargetType {
  var baseURL: URL {
    guard let url = URL(string: "https://api.dailymotion.com/") else {
      SLog.e("Cannot create URL")
      fatalError()
    }
    return url
  }
  
  var path: String {
    switch self {
    case .getUsers:
      return "users"
    }
  }
  
  var method: Moya.Method {
    switch self {
    case .getUsers:
      return .get
    }
  }
  
  var sampleData: Data {
    switch self {
    case .getUsers:
      return "{\"page\": 1, \"limit\": 10, \"explicit\" : false, \"total\": 1000, \"has_more\" : true, \"list\": [ { \"avatar_360_url\" : \"https://s2.dmcdn.net/d/2000001lFZ7PI/360x360\", \"username\" : \"dm_0930432ad3543533cb7e0ade6de53217\" }]}".utf8Encoded
    }
  }
  
  var task: Task {
    switch self {
    case .getUsers:
      let params = "avatar_360_url,username"
      guard let encoding = params.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
        SLog.e("Wrong encoding")
        fatalError()
      }
      return .requestParameters(parameters: ["fields": encoding], encoding: URLEncoding.default)
    }
  }
  
  var headers: [String : String]? {
    switch self {
    case .getUsers:
      return .none
    }
  }
}
