//
//  GitHubApi.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 01/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import Moya

/**
 Provider to requesting GitHub Api
 */
typealias GitHubApiProvider = MoyaProvider<GitHubApi>

/**
 This target provide all cases to uses GitHub Api
 */
enum GitHubApi {
  case getUser
}

extension GitHubApi: TargetType {
  var baseURL: URL {
    guard let url = URL(string: "https://api.github.com/") else {
      SLog.e("Cannot create URL")
      fatalError()
    }
    return url
  }
  
  var path: String {
    switch self {
    case .getUser:
      return "users"
    }
  }
  
  var method: Moya.Method {
    switch self {
    case .getUser:
      return .get
    }
  }
  
  var sampleData: Data {
    switch self {
    case .getUser:
      return "[{\"login\": \"mojombo\",\"id\": 1, \"node_id\": \"MDQ6VXNlcjE=\", \"avatar_url\" : \"https://avatars0.githubusercontent.com/u/1?v=4\", \"gravatar_id\" : \"\", \"url\" : \"https://api.github.com/users/mojombo\", \"html_url\" : \"https://github.com/mojombo\", \"followers_url\" : \"https://api.github.com/users/mojombo/followers\", \"following_url\" : \"https://api.github.com/users/mojombo/following{/other_user}\" , \"gists_url\" : \"https://api.github.com/users/mojombo/gists{/gist_id}\", \"starred_url\" : \" https://api.github.com/users/mojombo/starred{/owner}{/repo} \", \"subscriptions_url\" : \"https://api.github.com/users/mojombo/subscriptions\", \"organizations_url\" : \"https://api.github.com/users/mojombo/orgs\", \"repos_url\" : \"https://api.github.com/users/mojombo/repos\", \"events_url\" : \"https://api.github.com/users/mojombo/events{/privacy}\", \"received_events_url\" : \"https://api.github.com/users/mojombo/received_events\", \"type\" : \"User\", \"site_admin\" : false}]".utf8Encoded
    }
  }
  
  var task: Task {
    switch self {
    case .getUser:
      return .requestPlain
    }
  }
  
  var headers: [String: String]? {
    switch self {
    case .getUser:
      return .none
    }
  }
}
