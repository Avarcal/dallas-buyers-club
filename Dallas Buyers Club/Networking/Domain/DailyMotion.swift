//
//  DailyMotion.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 30/09/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation

// MARK: - DailyMotion
/**
 Represents response object from request to DailyMotion Api
 */
struct DailyMotion: Codable {
  let page, limit: Int
  let explicit: Bool
  let total: Int
  let hasMore: Bool
  let list: [DailyMotionUser]
  
  enum CodingKeys: String, CodingKey {
    case page, limit, explicit, total
    case hasMore = "has_more"
    case list
  }
}

// MARK: - DailyMotionUser
/**
 Represents single user on list in DailyMotion Api response
 */
struct DailyMotionUser: Codable {
  let avatarUrl: String
  let username: String
  
  enum CodingKeys: String, CodingKey {
    case avatarUrl = "avatar_360_url"
    case username
  }
}

extension DailyMotionUser: DallasBuyer {
  var name: String {
    return username
  }
  
  var avatarPathURL: String {
    return avatarUrl
  }
  
  var source: String {
    return "DailyMotion"
  }
  
}
