//
//  GitHub.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 30/09/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import Kingfisher

/**
 List with users from GitHub
 */
typealias GitHubUsers = [GitHubUser]

/**
 Represents response object from request to GitHub Api
 */
struct GitHubUser: Codable {
    let login: String
    let id: Int
    let nodeID: String
    let avatarURL: String
    let gravatarID: String
    let url, htmlURL, followersURL: String
    let followingURL, gistsURL, starredURL: String
    let subscriptionsURL, organizationsURL, reposURL: String
    let eventsURL: String
    let receivedEventsURL: String
    let type: AccountType
    let siteAdmin: Bool

    enum CodingKeys: String, CodingKey {
        case login, id
        case nodeID = "node_id"
        case avatarURL = "avatar_url"
        case gravatarID = "gravatar_id"
        case url
        case htmlURL = "html_url"
        case followersURL = "followers_url"
        case followingURL = "following_url"
        case gistsURL = "gists_url"
        case starredURL = "starred_url"
        case subscriptionsURL = "subscriptions_url"
        case organizationsURL = "organizations_url"
        case reposURL = "repos_url"
        case eventsURL = "events_url"
        case receivedEventsURL = "received_events_url"
        case type
        case siteAdmin = "site_admin"
    }
}

extension GitHubUser: DallasBuyer {
  var name: String {
    return login
  }
  
  var source: String {
    return "GitHub"
  }
  
  var avatarPathURL: String {
    return avatarURL
  }
  
}

/**
 Define GitHub user account type
 */
enum AccountType: String, Codable {
    case organization = "Organization"
    case user = "User"
}
