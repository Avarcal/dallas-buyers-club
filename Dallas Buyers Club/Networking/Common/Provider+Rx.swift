//
// Copyright (C) 2017 Tootbot Contributors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import Moya
import RxSwift

public extension Reactive where Base: MoyaProviderType {
  
  /// Designated request-making method.
  ///
  /// - Parameters:
  ///   - token: Entity, which provides specifications necessary for a `MoyaProvider`.
  ///   - callbackQueue: Callback queue. If nil - queue from provider initializer will be used.
  /// - Returns: Single response object.
  //
  //    public func request(_ target: SubTargetType, queue: DispatchQueue?, progress: Moya.ProgressBlock? = nil, completion: @escaping Moya.Completion) -> Cancellable {
  //        return base.rxRequest(DynamicTarget(baseURL: URL(string: ""), subTarget: target), callbackQueue: queue, progress: progress, completion: completion)
  //    }
  
  func request(_ token: Base.Target, callbackQueue: DispatchQueue? = nil) -> Single<Response> {
    return base.rxRequest(token, callbackQueue: callbackQueue)
  }
  
  /// Designated request-making method with progress.
  func requestWithProgress(_ token: Base.Target, callbackQueue: DispatchQueue? = nil) -> Observable<ProgressResponse> {
    return base.rxRequestWithProgress(token, callbackQueue: callbackQueue)
  }
}

extension MoyaProviderType {
  
  func rxRequest(_ token: Target, callbackQueue: DispatchQueue? = nil) -> Single<Response> {
    return Single.create { [weak self] single in
      let cancellableToken = self?.request(token, callbackQueue: callbackQueue, progress: nil) { result in
        switch result {
        case let .success(response):
          single(.success(response))
        case let .failure(error):
          single(.error(error))
        }
      }
      
      return Disposables.create {
        cancellableToken?.cancel()
      }
    }
  }
  
  internal func rxRequestWithProgress(_ token: Target, callbackQueue: DispatchQueue? = nil) -> Observable<ProgressResponse> {
    let progressBlock: (AnyObserver) -> (ProgressResponse) -> Void = { observer in
      return { progress in
        observer.onNext(progress)
      }
    }
    
    let response: Observable<ProgressResponse> = Observable.create { [weak self] observer in
      let cancellableToken = self?.request(token, callbackQueue: callbackQueue, progress: progressBlock(observer)) { result in
        switch result {
        case .success:
          observer.onCompleted()
        case let .failure(error):
          observer.onError(error)
        }
      }
      
      return Disposables.create {
        cancellableToken?.cancel()
      }
    }
    
    // Accumulate all progress and combine them when the result comes
    return response.scan(ProgressResponse()) { last, progress in
      let progressObject = progress.progressObject ?? last.progressObject
      let response = progress.response ?? last.response
      return ProgressResponse(progress: progressObject, response: response)
    }
  }
}

