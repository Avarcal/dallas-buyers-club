//
//  Codable+BDC.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 02/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation

extension Decodable {
  
  /**
   Returns Decodable object from data
   
   - parameter decoder: JSONDecoder to decode JSON
   - parameter data: Data to create decodable object
   
   - Returns: Decocable object
   */
  static func decode(with decoder: JSONDecoder = JSONDecoder(), from data: Data) -> Self? {
    do {
      let object = try decoder.decode(Self.self, from: data)
      return object
    } catch {
      return nil
    }
  }  
}
