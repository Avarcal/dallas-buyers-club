//
//  String+UTF8.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 30/09/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation

extension String {
  
  /**
   Returns a Data containing a representation of the String encoded using a encoding UTF8.
   */
  var utf8Encoded: Data {
    guard let data = data(using: .utf8) else {
      fatalError()
    }
    return data
  }
}
