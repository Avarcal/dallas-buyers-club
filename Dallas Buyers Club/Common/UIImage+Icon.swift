//
//  UIImage+Icon.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 05/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
  /**
   Return DailyMotion logo from internal resources
   
   - return: DailyMotion logo from internal resources
   */
  open class func dailyMotion() -> UIImage? {
    return self.init(named: "daily")
  }
  
  /**
   Return GitHub logo from internal resources
   
   - return: GitHub logo from internal resounces
   */
  open class func gitHub() -> UIImage? {
    return self.init(named: "github-logo")
  }
}
