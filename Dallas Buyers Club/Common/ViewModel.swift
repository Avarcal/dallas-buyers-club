//
//  ViewModel.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 04/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation

protocol ViewModel {
  associatedtype Input
  associatedtype Output
  
  func transform(input: Input) -> Output
}
