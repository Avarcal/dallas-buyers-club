//
//  UIViewController+Storyboard.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 04/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import UIKit

fileprivate enum Storyboards {
  static let main = "Main"
}

extension UIViewController {
  static func fromStoryboard() -> UIViewController {
    let bundle = Bundle(for: self)
    let identifier = String(describing: self)
    
    return UIStoryboard(name: Storyboards.main, bundle: bundle).instantiateViewController(withIdentifier: identifier)
  }
}
