//
//  Log.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 30/09/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation

/**
 Custom implemetation for console log
 It is possible to change print for e.g SwiftyBeaver
 */
class SLog {
  
  /**
   Debug
   */
  static func d(_ format: String) {
    #if DEBUG
    print("🍺 - \(format)")
    #endif
  }
  
  /**
   Error
   */
  static func e(_ format: String) {
    #if DEBUG
    print("🐪 - \(format)")
    #endif
  }
  
  /**
   Info
   */
  static func i(_ format: String) {
    #if DEBUG
    print("🦴 - \(format)")
    #endif
  }
}
