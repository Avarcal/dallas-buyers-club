//
//  MemberDetailViewController.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 05/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class MemberDetailViewController: UIViewController {
  
  @IBOutlet weak var avatarImage: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var sourceImageView: UIImageView!
  
  private var viewModel: MemberCellViewModel?
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
  }
  
  private func setupView() {
    if let url = viewModel?.avatarURL {
      avatarImage.kf.setImage(with: url)
    }
    nameLabel.text = viewModel?.name
    sourceImageView.image = viewModel?.source == "DailyMotion"
      ? UIImage.dailyMotion()
      : UIImage.gitHub()
  }
  
  /**
   Inject view model object to view controller
   
   -parameter viewModel: view model dependencies for MemberDetailViewController
   */
  func set(_ viewModel: MemberCellViewModel) {
    self.viewModel = viewModel
  }
}
