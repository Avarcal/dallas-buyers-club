//
//  BuyersNavigationViewController.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 04/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import UIKit

class BuyersNavigationViewController: UINavigationController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let root = DallasBuyersViewController.fromStoryboard()
    guard let vc = root as? DallasBuyersViewController else {
      fatalError("Something went wrong")
    }
    let service = MembersService()
    let vm = DallasBuyersViewModel(service: service)
    vc.set(vm)
    
    self.setViewControllers([vc], animated: false)
  }
}
