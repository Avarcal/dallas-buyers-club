//
//  MembersService.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 02/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import RxSwift

/**
 Interface for MembersService
 */
protocol MembersServiceType {
  /**
   Returns members from GitHub and DailyMotion as ViewModels
   
   - Returns: Table with ViewModels from user from GitHub and DailyMotion
   */
  func getMembers() -> Observable<[MemberCellViewModelable]>
}

/**
 Service to calling GitHub API and DailyMotion API
 */
class MembersService {
  
  let gitHubMembers: GitHubServiceType
  let dailyMotionMembers: DailyMotionServiceType
  
  init(gitHub: GitHubServiceType = GitHubService(),
       dailyMotion: DailyMotionServiceType = DailyMotionService()) {
    self.gitHubMembers = gitHub
    self.dailyMotionMembers = dailyMotion
  }
}

extension MembersService: MembersServiceType {
  /**
    Returns members from GitHub and DailyMotion as ViewModels
    
    - Returns: Table with ViewModels from user from GitHub and DailyMotion
    */
  func getMembers() -> Observable<[MemberCellViewModelable]> {
    
    let gitMembers = gitHubMembers
      .get()
      .map { members -> [MemberCellViewModelable] in
        return members.map { (member) -> MemberCellViewModelable in
          return MemberCellViewModel(buyer: member)
        }
    }
    
    let dailyMembers = dailyMotionMembers
      .get()
      .map { (members) -> [MemberCellViewModelable] in
        return members.list.map { (member) -> MemberCellViewModelable in
          return MemberCellViewModel(buyer: member)
        }
    }
    let merge = Observable.of(gitMembers, dailyMembers).merge()
    return merge
    
  //  return Observable.merge(dailyMembers, gitMembers)
  }
}
