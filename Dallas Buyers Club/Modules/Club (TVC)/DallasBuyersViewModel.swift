//
//  DallasBuyersViewModel.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 05/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

/**
 View model designed for DallasBuyerViewController.
 Serves view models to view controller
 */
class DallasBuyersViewModel: ViewModel {
  
  private let service: MembersServiceType
  private let disposeBag = DisposeBag()
  
  private var members: [MemberCellViewModelable]?
  
  struct Input {
    let trigger: Driver<Void>
    let selection: Driver<IndexPath>
  }
  
  struct Output {
    let cellViewModels: Driver<[MemberCellViewModelable]>
  }
  private let cellViewModelsSubject = PublishSubject<[MemberCellViewModelable]>()
  
  init(service: MembersServiceType) {
    self.service = service
  }
  
  /**
   Transform inputs to outputs
   */
  func transform(input: DallasBuyersViewModel.Input) -> DallasBuyersViewModel.Output {
    
    let viewModels = input
      .trigger
      .flatMapLatest { [unowned self] in
        return self.service
          .getMembers()
          .asDriver(onErrorJustReturn: [])
    }
    
    return Output(cellViewModels: viewModels)
  }
}
