//
//  DallasBuyersViewController.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 30/09/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DallasBuyersViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  private var viewModel: DallasBuyersViewModel?
  private let refreshControl = UIRefreshControl()
  private let disposeBag = DisposeBag()
  
  //MARK: - Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configureRefresh()
    bind()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
}

//MARK: - Configuration
extension DallasBuyersViewController {
  /**
   Inject view model object to view controller
   
   -parameter viewModel: view model dependencies for DallasBuyersViewControler
   */
  func set(_ viewModel: DallasBuyersViewModel) {
    self.viewModel = viewModel
  }
  
  fileprivate func configureRefresh() {
    tableView.refreshControl = refreshControl
  }
}

//MARK: - ViewModel Binding
extension DallasBuyersViewController {
  private func bind() {
    guard let viewModel = viewModel,
      let refreshableTableView = tableView.refreshControl else {
        SLog.e("View model is nil or TableView dosen't have bind refresh control")
        return
    }
    let viewWillAppear = rx
      .sentMessage(#selector(UIViewController.viewWillAppear(_:)))
      .map { (_) -> Void in
        return Void()
    }
    .asDriver(onErrorJustReturn: ())
    
    let pull = refreshableTableView
      .rx
      .controlEvent(.valueChanged)
      .asDriver()
    
    let input = DallasBuyersViewModel.Input(
      trigger: Driver.merge(viewWillAppear, pull),
      selection: tableView.rx.itemSelected.asDriver()
    )
    
    let output = viewModel.transform(input: input)
    
    output
      .cellViewModels
      .do(onNext: { [unowned self] (_) in
        self.refreshControl.endRefreshing()
      })
      .drive(
        tableView
          .rx
          .items(
            cellIdentifier: MemberCell.name,
            cellType: MemberCell.self
        )
      ) { (row, element, cell) in
        cell.set(element)
        SLog.i("\(element)")
    }
    .disposed(by: disposeBag)
    
    tableView
      .rx
      .modelSelected(MemberCellViewModel.self)
      .subscribe(onNext: { [unowned self] (cell) in
        SLog.d("tapped: \(cell)")
        self.showDetails(cell)
      })
      .disposed(by: disposeBag)
  }
}

//MARK: - Navigation
extension DallasBuyersViewController {
  private func showDetails(_ viewModel: MemberCellViewModel) {
    let dvc = MemberDetailViewController.fromStoryboard()
    guard let vc = dvc as? MemberDetailViewController else {
      fatalError("Someting went wrong")
    }
    vc.set(viewModel)
    self.present(vc, animated: true, completion: {})
  }
}
