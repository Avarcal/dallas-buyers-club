//
//  MebmerCell.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 04/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import Kingfisher
import UIKit

/**
 Custom cell with representation of Dallas Buyer
 */
class MemberCell: UITableViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var sourceIconImageView: UIImageView!
  
  func set(_ viewModel: MemberCellViewModelable) {
    
    if let url = viewModel.avatarURL {
     avatarImageView.kf.setImage(with: url)
    }
    nameLabel.text = viewModel.name
    sourceIconImageView.image = viewModel.source == "DailyMotion"
      ? UIImage.dailyMotion()
      : UIImage.gitHub()
  }
}

extension MemberCell {
  static var name: String {
    return "MemberCell"
  }
}
