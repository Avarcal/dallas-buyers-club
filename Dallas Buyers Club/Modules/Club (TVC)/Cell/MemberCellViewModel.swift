//
//  MemberCellViewModel.swift
//  Dallas Buyers Club
//
//  Created by Kamil Strąk on 02/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

/**
 Interface for represents MemberCellViewModel
 */
protocol MemberCellViewModelable {
  var name: String { get }
  var avatarURL: URL? { get }
  var source: String { get }
}

/**
 View Model for cell witch represents member
 */
struct MemberCellViewModel: MemberCellViewModelable {
  var name: String
  var avatarURL: URL?
  var source: String
  
  init(buyer: DallasBuyer) {
    self.name = buyer.name
    self.avatarURL = URL(string: buyer.avatarPathURL)
    self.source = buyer.source
  }
}
