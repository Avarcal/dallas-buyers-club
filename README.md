# Dallas Buyers Club

![Language](https://img.shields.io/badge/Language-Swift%205-brightgreen)
![Cocoapods](https://img.shields.io/badge/Cocoapods-version%3A%201.8.1-blue) 
![iOS](https://img.shields.io/badge/iOS-%3E%3D%2010.0-blue)
---
Dallas Buyers Club is example application using Moya to calling two difference API provider.
Moya gives posibility to simple extension endpoints, create powerfull unit tests on networking layer.

Project are using RxSwift for more elegant implementation of architecture.

### Documentation
Project is prepared to create Markup Swift Documentation.
Is possible to generate documentation with <a href="https://github.com/realm/jazzy">Jazzy</a>

### Instalation

1. Checkout repository *select your branch*
2. Install external dependencies `pod instal` - You should use cocoapod in version 1.8.1
3. Run Application

