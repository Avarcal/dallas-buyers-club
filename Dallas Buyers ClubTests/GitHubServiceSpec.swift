//
//  GitHubServiceSpec.swift
//  Dallas Buyers ClubTests
//
//  Created by Kamil Strąk on 06/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Quick
import Nimble
import Moya
import Result
import RxSwift
import RxTest

@testable import Dallas_Buyers_Club

class GitHubServiceSpec: QuickSpec {
  
  override func spec() {
    describe("Connection to GitHub Api") {
      var sut: GitHubService?
      var stubProvider: GitHubApiProvider?
      let disposeBag = DisposeBag()
  
      context("when user get data from Github API") {
        beforeEach {
          stubProvider = MoyaProvider<GitHubApi>(stubClosure: MoyaProvider.immediatelyStub)
          sut = GitHubService(api: stubProvider!)
        }
        
        it("request returns GitHubUser object") {
          waitUntil { done in
                      sut?.get().subscribe(onNext: { (users) in
              expect(users).to(beAKindOf(GitHubUsers.self))
            })
              .disposed(by: disposeBag)
            
            done()
          }
        }
      }
    }
  }
}
