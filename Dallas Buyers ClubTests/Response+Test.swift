//
//  Response+Test.swift
//  Dallas Buyers ClubTests
//
//  Created by Kamil Strąk on 02/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import Moya

extension Response {
  
  static func notFound() -> Response {
    return Response(statusCode: 404, data: Data())
  }
  
  static func success() -> Response {
    return Response(statusCode: 200, data: Data())
  }
}
