//
//  DailyMotionServiceSpec.swift
//  Dallas Buyers ClubTests
//
//  Created by Kamil Strąk on 06/10/2019.
//  Copyright © 2019 Kamil Strąk. All rights reserved.
//

import Foundation
import Quick
import Nimble
import Moya
import Result
import RxSwift
import RxTest

@testable import Dallas_Buyers_Club

class DailyMotionServiceSpec: QuickSpec {
  
  override func spec() {
    describe("Connection to GitHub Api") {
      var sut: DailyMotionService?
      var stubProvider: DailyMotionApiProvider?
      let disposeBag = DisposeBag()
      
      context("when user get data from Github API") {
        beforeEach {
          stubProvider = MoyaProvider<DailyMotionApi>(stubClosure: MoyaProvider.immediatelyStub)
          sut = DailyMotionService(api: stubProvider!)
        }
        it("request returns GitHubUser") {
          waitUntil { done in
            sut?
              .get()
              .subscribe(onNext: { (users) in
                expect(users).to(beAKindOf(DailyMotion.self))
              })
              .disposed(by: disposeBag)
            done()
          }
        }
      }
    }
  }
}
